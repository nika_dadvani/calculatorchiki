package com.example.mypirveliapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.strictmode.CleartextNetworkViolation
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.pasuxi)
    }

    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()
            if (result == "=") {
                result = ""
            }
            resultTextView.text = result + number
        }

    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            val result = resultTextView.text.toString()
            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }

            operation = clickedView.text.toString()
            resultTextView.text = ""
            if (operation == "½") {
                resultTextView.text = (operand / 2).toString()
            }
        }
    }

    fun equalsClick(clickedView: View) {

        val secOperandText = resultTextView.text.toString()
        var secOperand: Double = 0.0
        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()
        }
        when (operation) {
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> resultTextView.text = (operand / secOperand).toString()
        }
    }

    fun Clear(clickedView: View) {
        resultTextView.text = ""
    }

    fun Delete(clickedView: View) {
        resultTextView.text = resultTextView.text.toString().dropLast(1)
    }

    fun Dot(clickedView: View){

        val result = resultTextView.text.toString()

        if ("." !in result && result.isNotEmpty()){
            resultTextView.text = "$result."
        }

    }
    fun powerTwo(clickedView: View) {
        val result = resultTextView.text.toString()
        resultTextView.text = Math.pow(result.toDouble(),2.0).toString()
    }
    fun fesvi(clickedView: View) {

        var result = resultTextView.text.toString()

        if (result.isNotEmpty()) {

            var sqrt = sqrt(result.toDouble())
            resultTextView.text = sqrt.toString()

        } else {
            Toast.makeText(applicationContext, "Enter number.", Toast.LENGTH_SHORT).show()
        }

    }
}